const express = require('express');
const router = express.Router();
const fs = require('fs');
const util = require('util'); 

// let fileContent = fs.readFileSync("userlist.json", "utf8");


router.get('/', (req, res) => {
    let fileContent = fs.readFileSync("output.json", "utf8");

    (fileContent) ? res.send(JSON.parse(fileContent)) : res.status(400).send(`Some error`);
});

router.post('/', (req, res) => {
    const { body } = req;
    try {
        let fileContent = fs.readFileSync("userlist.json", "utf8");
        const newObj = JSON.parse(fileContent);
        newObj.push(body);
        fs.writeFileSync('userlist.json', JSON.stringify(newObj), 'utf-8')
        fs.writeFileSync('output.json', JSON.stringify(newObj), 'utf-8');
        res.status(200).json(newObj);
    } catch(e) {
        res.status(500).send('Some error');
    }
});


module.exports = router;


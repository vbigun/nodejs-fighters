const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');
const jsonfile = require('jsonfile'); 

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const userRouter = require('./routes/user');

let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/user',  userRouter);
app.get('/user/:id', (req, res, next) => {
    console.log('ID:', req.params.id);
    next();
  }, (req, res, next) => {
    let fileContent = fs.readFileSync("output.json", "utf8");
    const newArr = JSON.parse(fileContent);
    let arrObj = newArr.find(el => el._id === req.params.id);
    (req.params.id) ? res.send(arrObj) : res.status(400).send(`Some error`);
  });
  app.delete('/user/:id', (req, res, next) => {
    console.log('ID:', req.params.id);
    next();
  },  (req, res, next) => {
    let fileContent = fs.readFileSync("userlist.json", "utf8");
    const newArr = JSON.parse(fileContent);
    let arrObj = newArr.filter(el => el._id !== req.params.id);
    fs.writeFileSync('userlist.json', JSON.stringify(arrObj), 'utf-8')
    if(req.params.id) {
        fs.writeFileSync('output.json', JSON.stringify(arrObj), 'utf-8');
        res.status(200).json(arrObj);
    } else {
        res.status(400).send(`Some error`);
    }
  });

app.put('/user/:id',  (req, res, next) => {
    console.log('ID:', req.params.id);
    next();
  },  (req, res, next) => {
        const { body } = req;
        let fileContent = fs.readFileSync("userlist.json", "utf8");
        let newArr = JSON.parse(fileContent);
        let i = req.params.id;

        newArr[i - 1] = body;
        // fs.writeFileSync('userlist.json', JSON.stringify(newArr), 'utf-8');

        if(req.params.id) {
            fs.writeFileSync('output.json', JSON.stringify(newArr), 'utf-8');
            res.status(200).json(newArr);
        } else {
            res.status(400).send(`Some error`);
        }
  });

module.exports = app;
